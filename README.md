# Mars
La programmation Java est un langage de programmation orienté objet populaire et largement utilisé dans l'industrie informatique. Java est célèbre pour sa portabilité, ce qui signifie que les programmes écrits en Java peuvent être exécutés sur de nombreuses plates-formes différentes, telles que Windows, Mac, Linux, Android, etc. La syntaxe de Java est simple et facile à comprendre pour les débutants, et le langage offre de nombreuses fonctionnalités avancées pour les programmeurs expérimentés. Java est également connu pour sa sécurité renforcée et son environnement d'exécution sûr, ce qui en fait un choix populaire pour les applications Web et mobiles.

En outre, Java est souvent utilisé dans la conception de systèmes de gestion de bases de données, de systèmes de trading et d'autres applications à haute performance. En somme, la programmation Java offre une grande polyvalence et une grande flexibilité pour les programmeurs de tous niveaux et est une compétence précieuse pour les professionnels de l'informatique.

# 2 Jeux

Le jeu par default est celui ou vous pourrez deplacer des drones et des robots sur une carte pre definie.
Le Deuxieme jeu, disponnible dans le fichier Extended, permet la creation de carte via differentes options.

# Comment jouer?

Pour jouer au jeu par default positionnez vous dans le repertoire courrant et tapez dans votre terminal ./gradlew run
Cela va compiler et lancer le jeu par default.

Pour creer des cartes positionnez vous dans le repertoire Extended.
Faites de meme ./gradlew run
En cliquant sur File, plusieurs options s'offrent a vous:
    -Charger une carte via une declaration Java: sample1 ou sample2
    -Charger a partir d'un String: exemple GGGGDGGGGxGGGGGGDGGxGGRCGGBGGxGRGRGGGGGxGGGGGGGGGxGGGGGGGCGxGDGDGGGGGxGGGCGDBGGxGRGGGGGGGx
    -Le load d'une carte compressée n'est pas encore disponnible mais l'export en compressé oui.

# Ce projet
Dans ce petit projet Java nous allons aborder l'ensemble des bases de ce langage de programmation.
